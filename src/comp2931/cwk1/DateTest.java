package comp2931.cwk1;

import org.hamcrest.CoreMatchers;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import org.junit.*;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.Calendar;
import static org.junit.Assert.*;

import static org.mockito.Matchers.*;

@RunWith(PowerMockRunner.class)
@PrepareForTest({Date.class})
/**
 * Created by sc16km on 31/10/17.
 */
public class DateTest {
    private Date dateString = new Date(2010,8, 28);
    private Date newdate = new Date(2000, 1, 1);



    /**
     * Test that the default constructor correctly sets to the current date
     *
     * <p>In order to test the current date which changes, I created a mock environment
     * to set the calendar date of the system as 12/02/2017 and the date constructor
     * can then be used to test if this is the correct date.</p>
     */
    @Test
    public void DateTest(){
        Calendar currentTestDate = Calendar.getInstance();
        currentTestDate.set(2017, Calendar.FEBRUARY, 12);
        PowerMockito.mockStatic(Calendar.class);
        Mockito.when(Calendar.getInstance()).thenReturn(currentTestDate);

        Date dateTest = new Date();

        Assert.assertThat(dateTest.getYear(), CoreMatchers.is(2017));
        Assert.assertThat(dateTest.getMonth(), CoreMatchers.is(02));
        Assert.assertThat(dateTest.getDay(), CoreMatchers.is(12));
    }

    /**
     * Test that the default constructor fails when a number is entered for the month
    */
    @Test
    public void DateConstructorMonthFail(){
        Calendar currentTestDate = Calendar.getInstance();
        currentTestDate.set(2017, 02, 12);
        PowerMockito.mockStatic(Calendar.class);
        Mockito.when(Calendar.getInstance()).thenReturn(currentTestDate);

        Date dateTest = new Date();

        Assert.assertFalse(dateTest.getMonth() == 02);
    }

    /**
     * Test that the toString class works correctly
     */
    @Test
    public void dateToString(){
        Assert.assertThat(dateString.toString(), CoreMatchers.is("2010-08-28"));
    }

    /**
     * Test that the toString class does not display in incorrect format
     */
    @Test
    public void dateToIncorrectString(){
        Assert.assertFalse(dateString.toString().equals("28-08-2010"));
    }
    /**
     * Test that an error message is displayed if years is out of acceptable bounds
     * Fist test for a year that is negative
     * Second test a date that is not in the near future
     */
    @Test(expected=IllegalArgumentException.class)
    public void yearsOutOfBound(){
        new Date(-2017, 1, 12);
        new Date(100000, 10, 20);
    }

    /**
     * Test that an error message is displayed for month out of bound
     * First test for a month less than one
     * Second test a month that does not exist
     */
    @Test(expected = IllegalArgumentException.class)
    public void monthsOutOfBound(){
        new Date(2017, -1, 30);
        new Date(2011, 13, 30);
    }

    /**
     * Test that an error message is displayed if the day is incorrect
     * Test for a month that is supposed to have less than 31 days
     * Test for a month that is supposed to have less than 30 days
     * Test for a month that is supposed to have less than 28 days
     */
    @Test(expected = IllegalArgumentException.class)
    public void daysOutOfBound(){
        new Date(2017, 1, 40);
        new Date(2017, 4, 31);
        new Date(2017, 2, 30);
    }

    /**
     * Test that the equals function works correctly
     */
    @Test
    public void equality() {
        Assert.assertTrue(newdate.equals(newdate));
        Assert.assertTrue(newdate.equals(new Date(2000, 1, 1)));
    }
    /**
     *Test that if one of the elements of the date is incorrect it fails
     */
    @Test
    public void incorrectEquality(){
        Assert.assertFalse(newdate.equals(new Date(2001, 1, 1)));
        Assert.assertFalse(newdate.equals(new Date(2000, 2, 1)));
        Assert.assertFalse(newdate.equals(new Date(2000, 1, 2)));
        Assert.assertFalse(newdate.equals("2000-01-01"));

    }

    /**
     * Test the getDayOfYear function for a date early in the year and one later
     */
    @Test
    public void dayOfYear(){
        Date earlyDate = new Date(2010, 1, 10);
        Assert.assertThat(earlyDate.getDayOfYear(), CoreMatchers.is(10));
        Date lateDate = new Date(2017, 11, 10);
        Assert.assertThat(lateDate.getDayOfYear(), CoreMatchers.is(314));
    }

    /**
     * Test that the leap years are correctly handled
     *
     * <p>Test that a correct leap year date is allowed and that an incorrect
     * one fails. Also check, for a correct leap year date, the toString and
     * getDayOfYear functions work correctly.</p>
     */
    @Test
    public void testLeapYear(){
        Date leapYear = new Date(2016, 02, 29);
        Assert.assertThat(leapYear.toString(), CoreMatchers.is("2016-02-29") );
        Assert.assertThat(leapYear.getDayOfYear(), CoreMatchers.is(60));
    }
    @Test(expected = IllegalArgumentException.class)
    public void testIncorrectLeapYear(){
        new Date(2011, 1, 29);
    }

}