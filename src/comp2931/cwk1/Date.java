// Class for COMP2931 Coursework 1

package comp2931.cwk1;
import org.omg.CORBA.Environment;

import java.util.Calendar;
import java.util.GregorianCalendar;


/**
 * Simple representation of a date.
 */
public class Date {

  private int year;
  private int month;
  private int day;
  private static final int MONTHS_PER_YEAR = 11;
  private  Calendar currentCalendar;

  /**
   * Creates a date using the given values for year, month and day.
   *
   * @param y Year
   * @param m Month
   * @param d Day
   */
  public Date(int y, int m, int d) {
      //Check if this year is a leap year
      boolean isLeapYear = isLeapYear(y);

      set(y, m, d, isLeapYear);
  }
  /**
   * Constructor for date, create object representing todays date
   */
  public Date(){
    //Create calendar with todays date
    currentCalendar = Calendar.getInstance();
    //Set variables
    year = currentCalendar.get(Calendar.YEAR);
    month = currentCalendar.get(Calendar.MONTH)+1;
    day = currentCalendar.get(Calendar.DAY_OF_MONTH);
  }
  /**
   * Returns the year component of this date.
   *
   * @return Year
   */
  public int getYear() {
    return year;
  }

  /**
   * Returns the month component of this date.
   *
   * @return Month
   */
  public int getMonth() {
    return month;
  }

  /**
   * Returns the day component of this date.
   *
   * @return Day
   */
  public int getDay() {
    return day;
  }

  /**
   * Provides a string representation of this date.
   *
   * ISO 8601 format is used (YYYY-MM-DD).
   *
   * @return Date as a string
   */
  @Override
  public String toString() {
    //Add one to month as we would expect 01 to be january not 00
    return String.format("%04d-%02d-%2d", year, month, day);
  }

  //Helper method to assign values to instance variables for dates

  private void set(int y, int m, int d, boolean leapYear){
    //Year should not be less than 0 and should not be greater than 9999 for displaying in string
    if(y < 0 || y > 9999){
      throw new IllegalArgumentException("Years out of range");
    }
    //Month should not be less than zero or greater than 11 (since java considers 0 as January)
    else if(m<0 || m> MONTHS_PER_YEAR) {
      throw new IllegalArgumentException("Months out of range");
    }
    else{
      if((m==3 || m==5 || m==8 || m==10 ) && (d > 30)){
        throw new IllegalArgumentException("Days out of range for this month");
      }
      else if((m==0 || m==2 || m==4 || m==6 || m==7 || m==9|| m==11) && (d > 31)){
        throw new IllegalArgumentException("Days out of range for this month");
      }
      //If it is a leap year then the days should allow for 29
      else if(m == 1 && leapYear == true && d >29) {
          throw new IllegalArgumentException("Days out of range for this month (Leap year)");
      }
      else if(m ==1 && leapYear == false && d>28){
          throw new IllegalArgumentException("Days out of range for this month (Not a leap year)");
      }
      else{
        year = y;
        month = m;
        day = d;
      }
    }
  }

  /**
   * Tests whether this date is equal to another.
   *
   * <p>The two objects are considered equal if both are instances of
   * the Date class <em>and</em> both represent exactly the same
   * date.</p>
   *
   * @return true if this Time object is equal to the other, false otherwise
   */
  @Override
  public boolean equals(Object other) {
    if (other == this) {
      // 'other' is same object as this one!
      return true;
    }
    else if (! (other instanceof Date)) {
      // 'other' is not a Date object
      return false;
    }
    else {
      // Compare fields
      Date otherDate = (Date) other;
      return getYear() == otherDate.getYear()
              && getMonth() == otherDate.getMonth()
              && getDay() == otherDate.getDay();
    }
  }

  /**
   * Calculate the day of year that corresponds to the date
   *
   * @return integer day of the year
   */
  public int getDayOfYear(){
    //Create a calendar and set the date to the one entered
    GregorianCalendar calendarTest = new GregorianCalendar();
    calendarTest.set(Calendar.YEAR, year);
    calendarTest.set(Calendar.MONTH, month-1);
    calendarTest.set(Calendar.DAY_OF_MONTH, day);
    int Day_Of_Year = calendarTest.get(Calendar.DAY_OF_YEAR);
    return Day_Of_Year;

  }

  /**
   * Test if this year is a leap year according to rules of Gregorian calendar
   * @param y Year
   * @return Boolean result of if year is leap year or not
   */
  public boolean isLeapYear(int y){
    //Create a calendar to use the leap year method
    GregorianCalendar calendarLeapYear = new GregorianCalendar();

    return calendarLeapYear.isLeapYear(y);
  }


}

